#!/bin/sh

# Prends les images en "haute résolution" dans "imagesraw/" et les réduit pour qu'elles soient plus légères sur le site web.

mkdir images || true
rm images/*
mogrify -path images/ -filter Triangle -define filter:support=2 -thumbnail 1920 -unsharp 0.25x0.25+8+0.065 -dither None -posterize 136 -quality 82 -define jpeg:fancy-upsampling=off -define png:compression-filter=5 -define png:compression-level=9 -define png:compression-strategy=1 -define png:exclude-chunk=all -interlace none -colorspace sRGB -strip imagesraw/*.{png,jpg}
cp imagesraw/*.svg images/
